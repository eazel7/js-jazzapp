exports = module.exports = function(app, conf) {
    var modulesPath = conf.glue.modules.path;

    var fs = require('fs');
    var path = require('path');
    var modules = fs.readdirSync(modulesPath);
    var express = require('express');
    var sendIndex = function(req, res) {
        var returnUrl = req.originalUrl;
        if (!req.session.user) {
            returnUrl = '/login?returnTo=' + encodeURIComponent(returnUrl);
        }
        res.redirect('/?goTo=' + encodeURIComponent(returnUrl));
    };
    var allRoutes = {};
    var enabledModules = [];

    modules.forEach(function(dir) {
        var localPath = path.join(modulesPath, dir, 'local.js');
        var locals = {};

        if (fs.existsSync(localPath)) {
            locals = require(localPath);
        }

        if (!locals.disabled) {
            enabledModules.push({
                modulePath: path.join(modulesPath, dir),
                name: dir,
                locals: locals
            });
        }
    });

    enabledModules.forEach(function(enabledModule) {
        var bootstrapPath = path.join(enabledModule.modulePath, 'backend', 'bootstrap.js');

        if (fs.existsSync(bootstrapPath)) {
            require(bootstrapPath)({
                enabledModules: enabledModules
            });
        }
    });

    enabledModules.forEach(function(enabledModule) {
        var configurePath = path.join(enabledModule.modulePath, 'backend', 'configure.js');
        if (fs.existsSync(configurePath)) {
            require(configurePath)(app, conf);
        }

        var routesPath = path.join(enabledModule.modulePath, 'routes.js');

        if (fs.existsSync(routesPath)) {
            var moduleRoutes = enabledModule.routes = require(routesPath);

            for (var r in moduleRoutes) {
                allRoutes[r] = moduleRoutes[r];

                app.use(r, sendIndex);
            }
        }

        app.use(express.static(path.join(enabledModule.modulePath, 'frontend')));

    });

    app.get('/api/ui/routes', function(req, res) {
        res.json(allRoutes);
    });
};