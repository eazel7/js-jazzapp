angular.module('bag2').controller('PermissionsCtrl', function($scope, Permission) {
    $scope.permissions = Permission.list();
    $scope.remove = function(p) {
        p.$delete(function(p) {
            $scope.permissions.splice($scope.permissions.indexOf(p), 1);
        });
    };
    $scope.$on('new-permission', function(event, p) {
        $scope.permissions.push(p);
    });
}).controller('NewPermissionCtrl', function($scope, Permission) {
    $scope.save = function() {
        $scope.permission.$save(function() {
            $scope.alerts.push({
                type: 'success',
                message: 'New permission created'
            });
            $scope.$emit('new-permission', $scope.permission);
            $scope.permission = new Permission();
        });
    };
    $scope.permission = new Permission();
});