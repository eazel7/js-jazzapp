'use strict';

angular.module('bag2').controller('UsersCtrl', function($scope, User) {
    $scope.users = User.query();
    $scope.remove = function(u) {
        u.$delete(function() {
            $scope.users = User.query();
        });
    };
    $scope.$on('new-user', function() {
        $scope.users = User.query();
    });
}).controller('NewUserCtrl', function($scope, User) {
    $scope.save = function() {
        $scope.user.$save(function() {
            $scope.alerts.push({
                type: 'success',
                message: 'New user created'
            });
            $scope.user = new User();
            $scope.$emit('new-user');
        });
    };
    $scope.user = new User();
}).controller('EditUserCtrl', function($scope, User, UserPermission, Permission, $routeParams) {
    $scope.user = User.findByName({
        username: $routeParams.username
    });
    $scope.userPermissions = UserPermission.findByName({
        username: $routeParams.username
    }, function() {
        if (!$scope.userPermissions.permissions) $scope.userPermissions.permissions = [];
    });
    $scope.save = function () {
        $scope.user.$save();
        $scope.userPermissions.$save();
    };
}).controller('EditUserPermissionsCtrl', function($scope) {
    $scope.remove = function(p) {
        $scope.userPermissions.permissions.splice($scope.userPermissions.permissions.indexOf(p), 1);
    };
}).controller('AddPermissionCtrl', function($scope) {
    $scope.add = function() {
        $scope.userPermissions.permissions.push($scope.key);
        $scope.key = '';
    };
}).controller('ChangePasswordCtrl', function($scope, User, $routeParams, $http) {
    $scope.user = User.findByName({
        username: $routeParams.username
    });
    $scope.save = function () {
        $http.post('/api/admin/changePassword', {
            username: $routeParams.username,
            newPassword: $scope.newPassword
        })
        .success(function() {
            $scope.alerts.push({
                type:'success',
                message: 'Password changed successfuly'
            })
        });
    };
});