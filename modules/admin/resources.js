exports = module.exports = [{
    name: 'User',
    collectionName: 'users',
    url: '/users/:username',
    params: {
        username: '@username'
    },
    actions: {
        list: {
            urls: ['/users'],
            kind: 'find'
        },
        findByName: {
            kind: 'findOne'
        },
        save: {
            kind: 'findAndModify',
            allowed: ['admin.users']
        },
        delete: {
            kind: 'remove',
            allowed: ['admin.users']
        }
    }
}, {
    name: 'Permission',
    collectionName: 'permissions',
    url: '/permissions/:key',
    params: {
        key: '@key'
    },
    actions: {
        list: {
            urls: ['/permissions'],
            kind: 'find'
        },
        findByName: {
            kind: 'findOne'
        },
        save: {
            kind: 'findAndModify',
            allowed: ['admin.users']
        },
        delete: {
            kind: 'remove',
            allowed: ['admin.users']
        }
    }
}, {
    name: 'UserPermission',
    collectionName: 'users.permissions',
    url: '/users.permissions/:username',
    params: {
        username: '@username'
    },
    actions: {
        list: {
            urls: ['/users.permissions'],
            kind: 'find'
        },
        findByName: {
            kind: 'findOne'
        },
        save: {
            kind: 'findAndModify',
            allowed: ['admin.users']
        },
        delete: {
            kind: 'remove',
            allowed: ['admin.users']
        }
    }
}];