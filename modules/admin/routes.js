exports = module.exports = {
    '/admin': {
        templateUrl: '/views/admin/admin.html',
        controller: 'AdminCtrl',
        navSection: 'admin',
        allowed: ['admin.users'],
        title: 'Administration zone'
    },
    '/admin/users': {
        templateUrl: '/views/admin/users.html',
        controller: 'UsersCtrl',
        navSection: 'admin',
        allowed: ['admin.users'],
        title: 'Users administration',
        parents: ['/admin']
    },
    '/admin/users/:username': {
        templateUrl: '/views/admin/users/edit.html',
        controller: 'EditUserCtrl',
        navSection: 'admin',
        allowed: ['admin.users'],
        title: 'Edit user',
        parents: ['/admin', '/admin/users']
    },
    '/admin/users/:username/changePassword': {
        templateUrl: '/views/admin/users/changePassword.html',
        controller: 'ChangePasswordCtrl',
        navSection: 'admin',
        title: 'Change password',
        parents: ['/admin', '/admin/users']
    },
    '/admin/permissions': {
        templateUrl: '/views/admin/permissions.html',
        controller: 'PermissionsCtrl',
        navSection: 'admin',
        allowed: ['admin.users'],
        title: 'Permissions administration',
        parents: ['/admin']
    }
};