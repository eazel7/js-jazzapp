var path = require('path'),
    fs = require('fs');

exports = module.exports = function(options) {
    var allApps = [];
    var enabledModules = options.enabledModules;
    enabledModules.forEach(function(mod) {
        var appPath = path.join(mod.modulePath, 'app.js');
        if (fs.existsSync(appPath)) {
            var app = require(appPath);

            allApps.push(app);
        }
    });

    require('./locals').allApps = allApps;
};