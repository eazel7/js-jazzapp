exports = module.exports = function(app) {
    var allApps = require('./locals').allApps;

    app.get('/api/apps', function(req, res) {
        res.json(allApps);
    });
};