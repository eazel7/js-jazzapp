'use strict';

angular.module('bag2').controller('LoginCtrl', function($scope, $location, auth, $routeParams) {
    auth.checkLoginState();
    $scope.$watch('isLoggedIn', function(isLoggedIn) {
        if (isLoggedIn) {
            $location.url($routeParams.returnTo || '/');
        }
    });
    $scope.login = function() {
        auth.login($scope.username, $scope.password);
    };
});
