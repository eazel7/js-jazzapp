'use strict';

angular.module('bag2').factory('auth', function($http, $rootScope, $location) {
    function checkLoginState() {

        $http.get('/api/auth/isLoggedIn').success(function() {
            $rootScope.isLoggedIn = true;
        }).error(function() {
            $rootScope.isLoggedIn = false;
        });
    }
    
    $rootScope.$watch('isLoggedIn', function (isLoggedIn) {
       if (isLoggedIn) {
           $http.get('/api/auth/permissions').success(function (data) {
               $rootScope.permissions = data;
           });
       }  else {
           $rootScope.permissions = [];
       }
    });

    // Public API here
    return {
        checkLoginState: function() {
            checkLoginState();
        },
        logout: function() {
            $http.post('/api/auth/logout').success(function() {
                $rootScope.isLoggedIn = false;
            });
            $location.url('/');
        },
        login: function(username, password) {
            $http.post('/api/auth/login', {
                username: username,
                password: password
            }).success(function() {
                $rootScope.isLoggedIn = true;
            }).error(function() {
                $rootScope.isLoggedIn = false;
            });
        }
    };
});
