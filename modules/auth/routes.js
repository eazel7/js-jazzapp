exports = module.exports = {
    '/login?returnTo=:returnTo': {
        templateUrl: '/views/login.html',
        controller: 'LoginCtrl',
        navSection: '',
        title: 'Login'
    },
    '/login': {
        templateUrl: '/views/login.html',
        controller: 'LoginCtrl',
        navSection: '',
        title: 'Login'
    }
};