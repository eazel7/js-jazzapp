'use strict';

angular.module('bag2').controller('ListsCtrl', function($scope, List) {
    $scope.lists =List.query();
    $scope.$on('new-list', function(e, l) {
        $scope.lists.push(l);
    });
    $scope.delete = function (l) {
        l.$delete(function() {
            $scope.lists.splice($scope.lists.indexOf(l), 1);
        });
    };
}).controller('NewListCtrl', function($scope, List) {
    $scope.list = new List();
    
    $scope.save = function() {
        var l = $scope.list;
        l.$save();
        $scope.list = new List();
        $scope.$emit('new-list', l);
    };
}).controller('EditListCtrl', function($scope, List, $routeParams, $location) {
    $scope.list = List.findOne({_id: $routeParams._id}, function() {
        if (!$scope.list.items) $scope.list.items = [];
    });
    
    $scope.save = function() {
        $scope.list.$save();
    };
    
    $scope.removeItem = function (i) {
        $scope.list.items.splice($scope.list.items.indexOf(i), 1);
    };
    
    $scope.addNewItem = function () {
        $scope.list.items.push($scope.newItem);
        $scope.newItem = '';
    };
    
    $scope.delete = function () {
        var title = $scope.list.title;
        $scope.list.$delete(function() {
            $location.url('/lists');
            $scope.alerts.push ({
                type: 'success',
                message: 'List deleted (' + title + ')'
            });
        });
    };
});