exports = module.exports = [{
    name: 'List',
    collectionName: 'lists',
    url: '/lists/:_id',
    params: {
        _id: '@_id'
    },
    actions: {
        list: {
            urls: ['/lists'],
            kind: 'find',
            allowed: ['lists']
        },
        findOne: {
            urls: ['/lists', '/lists/:_id'],
            kind: 'findOne',
            allowed: ['lists']
        },
        save: {
            urls: ['/lists', '/lists/:_id'],
            kind: 'findAndModify',
            allowed: ['lists']
        },
        delete: {
            urls: ['/lists', '/lists/:_id'],
            kind: 'remove',
            allowed: ['lists']
        }
    }
}];