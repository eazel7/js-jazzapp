exports = module.exports = {
    '/lists': {
        templateUrl: '/views/lists/lists.html',
        controller: 'ListsCtrl',
        navSection: 'apps',
        title: 'Lists',
        allowed:['lists']
    },
    '/lists/:_id': {
        templateUrl: '/views/lists/edit.html',
        controller: 'EditListCtrl',
        navSection: 'apps',
        title: 'Lists',
        parents: ['/lists'],
        allowed:['lists']
    }
};