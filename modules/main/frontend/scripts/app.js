'use strict';

angular.module('bag2', ['ngUpload', 'ngResource', 'ui.directives', 'ui.bootstrap']).config(function($provide, $routeProvider, $locationProvider) {
    $locationProvider.html5Mode(true);

    $routeProvider.when('/', {
        templateUrl: '/views/main.html',
        controller: 'MainCtrl',
        navSection: 'home',
        title: 'Dashboard'
    }).otherwise({
        redirectTo: '/'
    });

    $provide.factory('$routeProvider', function() {
        return $routeProvider;
    });
}).run(function(auth, $routeProvider, $http, $route, $rootScope, $location) {
        $http.get('/api/ui/routes').success(function(routes) {
        $rootScope.uiRoutes = routes;
        for (var r in routes) {
            $routeProvider.when(r, routes[r]);
        }
        
        $rootScope.isAllowedTo = function(r) {
            if (!r) {
                return true;
            }
            
            var found = false;
            if (r.allowed && $rootScope.isLoggedIn && $rootScope.permissions) {
                $rootScope.permissions.forEach(function(p) {
                   r.allowed.forEach(function(p2) {
                       if (p == p2) {
                           found = true;
                       }
                   });
                });
                
                return found;
            } else if (!r.allowed) {
                return true;
            } else {
                return false;
            }
        };
        
        $rootScope.$on('$routeChangeStart', function (event, next, current) {
            if (!$rootScope.isAllowedTo(next)) {
                $location.url('/notAllowed?url=' + encodeURIComponent($location.url()));
                $location.replace();
            }
        });

        $route.reload();
    });
})
.controller('PageCtrl', function ($scope, auth) {
    $scope.alerts = [];

    auth.checkLoginState();
    $scope.$on('$routeChangeSuccess', function(e, newRoute) {
        $scope.pageTitle = 'BAG2 - ' + newRoute.title;

        $scope.breadcrumb = [];

        if (newRoute.parents) {
            newRoute.parents.forEach(function(p) {
                if ($scope.uiRoutes[p]) {
                    $scope.breadcrumb.push({
                        title: $scope.uiRoutes[p].title,
                        url: p
                    });
                }
            });
        }

        $scope.breadcrumb.push({
            current: true,
            title: newRoute.title
        });
    });
});

