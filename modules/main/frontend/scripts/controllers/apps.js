angular.module('bag2').controller('AppsCtrl', function($scope, $http) {
    $http.get('/api/apps').success(function(data) {
        $scope.apps = data;
    });
});