'use strict';

angular.module('bag2').controller('MainCtrl', function($scope, $location, $routeParams) {
    if ($routeParams.goTo) {
        $location.url($routeParams.goTo);
    }
});
