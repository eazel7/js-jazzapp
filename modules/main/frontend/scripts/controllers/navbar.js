function NavBarCtrl($scope, $location, auth) {
    $scope.login = function() {
        $location.url('/login');
    };
    $scope.logout = function() {
        auth.logout();
    };
    $scope.$on('$routeChangeSuccess', function(ev, current) {
        $scope.section = current.navSection;
    });
}