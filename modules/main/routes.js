exports = module.exports = {
    '/apps' :{
        title: 'Applications',
        navSection: 'apps',
        templateUrl: '/views/apps.html',
        controller: 'AppsCtrl',
        parents: []
    },
    '/notAllowed' :{
        title: 'Not allowed',
        navSection: '',
        templateUrl: '/views/notAllowed.html',
        controller: 'NotAllowedCtrl',
        parents: []
    }
};