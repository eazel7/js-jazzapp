angular.module('bag2').controller('MapsLayersCtrl', function($scope, MapLayer) {
    $scope.layers = MapLayer.list();
}).controller('EditMapLayerCtrl', function($scope, MapLayer, $routeParams) {
    $scope.layer = MapLayer.findOne({
        _id: $routeParams._id
    });
}).controller('NewMapLayerCtrl', function($scope, MapLayer, $location) {
    $scope.layer = new MapLayer();
    $scope.callbackFunction = function(contentOfInvisibleFrame) {
        alert(contentOfInvisibleFrame);
    };

    $scope.save = function() {
        $scope.layer.$save(function() {
            $location.url('/maps/layers/' + $scope.layer._id + '/edit');
        });
    };
});