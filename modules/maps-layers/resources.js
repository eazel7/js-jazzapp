exports = module.exports = [{
    name: 'MapLayer',
    collectionName: 'maps.layers',
    url: '/maps.layers/:_id',
    params: {
        _id: '@_id'
    },
    actions: {
        list: {
            urls: ['/maps.layers'],
            kind: 'find',
            allowed: ['maps.layers']
        },
        findOne: {
            urls: ['/maps.layers', '/maps.layers/:_id'],
            kind: 'findOne',
            allowed: ['maps.layers']
        },
        save: {
            urls: ['/maps.layers', '/maps.layers/:_id'],
            kind: 'findAndModify',
            allowed: ['maps.layers']
        },
        delete: {
            urls: ['/maps.layers', '/maps.layers/:_id'],
            kind: 'remove',
            allowed: ['maps.layers']
        }
    }
}];