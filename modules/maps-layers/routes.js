exports = module.exports = {
  '/maps/layers': {
      controller: 'MapsLayersCtrl',
      templateUrl: '/views/maps/layers.html',
      navSection: 'apps',
      title: 'Map layers',
      allowed: ['maps.layers']
  },
  '/maps/layers/new': {
      controller: 'NewMapLayerCtrl',
      templateUrl: '/views/maps/layers/new.html',
      navSection: 'apps',
      title: 'New layer',
      parents: ['/maps/layers'],
      allowed: ['maps.layers']
  },
  '/maps/layers/:_id/edit': {
      controller: 'EditMapLayerCtrl',
      templateUrl: '/views/maps/layers/edit.html',
      navSection: 'apps',
      title: 'Edit',
      parents: ['/maps/layers'],
      allowed: ['maps.layers']
  }
};