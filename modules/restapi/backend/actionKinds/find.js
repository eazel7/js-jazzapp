var oid = require('mongodb').ObjectID;

exports = module.exports = {
    method: 'get',
    isArray: true,
    process: function(resource, action, req, res, db) {
        if (!require('./common').checkAuth(action, req)) {
            res.status(403);
            res.end();
        }
        else {
            db.getDbInstance(function(err, db) {
                var noErr = require('./common').noErr;
                if (noErr(err, res)) {
                    var query = {};

                    if (resource.params) {
                        for (var p in resource.params) {
                            if (req.params[p]) {
                                query[p] = req.params[p];
                            }
                        }
                    }

                    if (query._id && typeof query._id == 'string') {
                        query._id = new oid(query._id);
                    }


                    db.collection(resource.collectionName || resource.name).find(query, function(err, results) {
                        if (noErr(err, res)) {
                            results.toArray(function(err, array) {
                                if (noErr(err, res)) {
                                    res.json(array);
                                }
                            });
                        }
                    });
                }
            });
        }
    }
};