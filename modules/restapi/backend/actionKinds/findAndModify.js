var oid = require('mongodb').ObjectID;

exports = module.exports = {
    method: 'post',
    isArray: false,
    process: function(resource, action, req, res, db) {
        if (!require('./common').checkAuth(action, req)) {
            res.status(403);
            res.end();
        }
        else {
            db.getDbInstance(function(err, db) {
                function handleErr(err) {
                    if (err) {
                        console.log(err);
                        res.status(503);
                        res.end();
                    }
                }
                handleErr(err);
                if (!err) {
                    var query = {};

                    if (resource.params) {
                        for (var p in resource.params) {
                            query[p] = req.params[p];
                        }
                    }

                    if (query._id && typeof query._id == 'string') {
                        // _id field should be of type ObjectID

                        query._id = new oid(query._id);
                    }
                    var item = JSON.parse(JSON.stringify(req.body));

                    // we cannot change the _id of a document
                    if (item._id) {
                        delete item._id;
                    }

                    db.collection(resource.collectionName || resource.name).findAndModify(query, {}, item, {
                        safe: true,
                        new: true,
                        upsert: true
                    }, function(err, doc) {
                        handleErr(err);
                        if (!err) {
                            res.json(doc);
                        }
                    });
                }
            });
        }
    }
};