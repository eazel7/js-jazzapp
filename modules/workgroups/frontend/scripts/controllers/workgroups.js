angular.module('bag2').controller('WorkgroupsCtrl', function($scope, Workgroup, $location) {
    $scope.workgroups = Workgroup.list(function() {}, function() {
        $scope.alerts.push({
            message: 'Failed to retrieve workgroups',
            type: 'danger'
        });
    });

    $scope.createNew = function() {
        $location.url('/workgroups/new');
    };
}).controller('NewWorkgroupCtrl', function($scope, Workgroup, $location) {
    $scope.workgroup = new Workgroup({
        members: []
    });

    $scope.save = function() {
        $scope.workgroup.save(function() {
            $location.url('/workgroups/' + $scope.workgroup._id + '/edit');
        });
    };
}).controller('WorkgroupMembersCtrl', function($scope, User) {
    $scope.allUsers = User.list();
    $scope.addMember = function() {
        $scope.workgroup.members.push($scope.memberName);
        $scope.memberName = '';
    };
}).controller('EditWorkgroupCtrl', function($scope, Workgroup, $routeParams) {
    $scope.workgroup = Workgroup.findOne({
        _id: $routeParams._id
    }, function() {
        if (!$scope.workgroup.members) {
            $scope.workgroup.members = [];
        }
    });

    $scope.save = function() {
        $scope.workgroup.save(function() {
            $scope.alerts.push({
                type: 'success',
                message: 'Workgroup saved successfuly'
            });
        }, function() {
            $scope.alerts.push({
                type: 'danger',
                message: 'Workgroup save failed'
            });
        });
    };
});