exports = module.exports = [{
    name: 'Workgroup',
    collectionName: 'workgroups',
    url: '/workgroups/:_id',
    params: {
        _id: '@_id'
    },
    actions: {
        list: {
            urls: ['/workgroups'],
            kind: 'find',
            allowed: ['workgroups']
        },
        findOne: {
            urls: ['/workgroups', '/workgroups/:_id'],
            kind: 'findOne',
            allowed: ['workgroups']
        },
        save: {
            urls: ['/workgroups', '/workgroups/:_id'],
            kind: 'findAndModify',
            allowed: ['workgroups']
        },
        delete: {
            urls: ['/workgroups', '/workgroups/:_id'],
            kind: 'remove',
            allowed: ['workgroups']
        }
    }
}];