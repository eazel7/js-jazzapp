exports = module.exports = {
  '/workgroups': {
      templateUrl: '/views/workgroups/index.html',
      controller: 'WorkgroupsCtrl',
      title: 'Workgroups',
      allowed: ['workgroups']
  },
  '/workgroups/new': {
      templateUrl: '/views/workgroups/new.html',
      controller: 'NewWorkgroupCtrl',
      title: 'New workgroups',
      parents: ['/workgroups'],
      allowed: ['workgroups']
  }
};