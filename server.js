var express = require("express"),
    http = require('http');

var nconf = require('./loadConfig')();
var MongoStore = require('connect-mongo')(express);

var store = new MongoStore({
    db: nconf.get('mongo:database'),
    host: nconf.get('mongo:hostname'),
    username: nconf.get('mongo:username'),
    password: nconf.get('mongo:password'),
    auto_reconnect: nconf.get('mongo:autoreconnect')
});

var app = express();

app.configure(function() {
    app.set('port', nconf.get('http:port'));

    if (process.env.PORT && process.env.PORT != nconf.get('http:port')) {
        var proxy = require('http-proxy');
        proxy.createServer(function(req, res, proxy) {
            proxy.proxyRequest(req, res, {
                host: req.headers.host.substring(0, req.headers.host.indexOf(':')),
                port: nconf.get('http:port')
            });
        }).listen(process.env.PORT, function() {
            console.log("Proxy server listening on port " + process.env.PORT);
        });
    }

    app.use(express.favicon());
    app.use(express.bodyParser({
        keepExtensions: true,
        uploadDir: __dirname + "/uploads"
    }));
    app.use(express.methodOverride());
    app.use(express.cookieParser(nconf.get('cookies:secret')));
    app.use(express.session({
        store: store,
        secret: nconf.get('session:secret'),
        key: nconf.get('session:key')
    }));
    app.use(express.errorHandler());
    app.use(app.router);

    require('./glue/configure')(app, nconf.get());
});


var server = http.createServer(app);

server.listen(app.get('port'), function() {
    console.log("Express server listening on port " + app.get('port'));
});